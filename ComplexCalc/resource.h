//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ComplexCalc.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_COMPLEXCALC_DIALOG          102
#define IDR_MAINFRAME                   128
#define RE1                             1000
#define IMAG1                           1001
#define RE2                             1003
#define IMAG2                           1004
#define RESULT1                         1005
#define RE3                             1006
#define IMAG3                           1007
#define COMBO2                          1008
#define RESULT2                         1009
#define THEBUTTON                       1010
#define IDC_RADIO1                      1011
#define IDC_RADIO2                      1012
#define IDC_RADIO3                      1013
#define IDC_RADIO4                      1014
#define IDC_BUTTON1                     1015
#define IDC_CLEARBUTTON                 1015

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
