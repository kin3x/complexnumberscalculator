#pragma once
#include "stdafx.h"
#include <iostream>
#include <math.h>

class ComplexNum
{
private:
	double m_nRealis;
	double m_nImaginalis;

public:
	ComplexNum();
	ComplexNum(double realis, double imaginalis);
	~ComplexNum();

	void SetRealis(double realis);
	void SetImaginalis(double imaginalis);

	double GetRealis() const;
	double GetImaginalis() const;

	double Modulus() const;				//modul liczby zespolonej
	ComplexNum Conjungate() const;		//liczba sprzezona
	CString ToLCString() const;

	friend ComplexNum operator + (const ComplexNum & left, const ComplexNum & right);
	friend ComplexNum operator - (const ComplexNum & left, const ComplexNum & right);
	friend ComplexNum operator * (const ComplexNum & left, const ComplexNum & right);
	friend ComplexNum operator / (const ComplexNum & left, const ComplexNum & right);

	friend ComplexNum & operator += (ComplexNum & left, const ComplexNum & right);
	friend ComplexNum & operator -= (ComplexNum & left, const ComplexNum & right);
	friend ComplexNum & operator *= (ComplexNum & left, const ComplexNum & right);
	friend ComplexNum & operator /= (ComplexNum & left, const ComplexNum & right);

	friend std::istream & operator >> (std::istream & s, ComplexNum & right);
	friend std::ostream & operator << (std::ostream & s, ComplexNum & right);

	ComplexNum & operator = (const ComplexNum & right)
	{
		m_nRealis = right.m_nRealis;
		m_nImaginalis = right.m_nImaginalis;

		return *this;
	}


};

