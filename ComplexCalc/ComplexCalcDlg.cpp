
// ComplexCalcDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ComplexCalc.h"
#include "ComplexCalcDlg.h"
#include "afxdialogex.h"
#include "ComplexNum.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CComplexCalcDlg dialog



CComplexCalcDlg::CComplexCalcDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_COMPLEXCALC_DIALOG, pParent)
	, m_sComboBoxValue(_T(""))
	, m_sRealis1(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CComplexCalcDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO1, m_bRadio1);
	DDX_CBString(pDX, COMBO2, m_sComboBoxValue);
	DDX_Control(pDX, COMBO2, m_bComboBox);
	DDX_Text(pDX, RE1, m_sRealis1);
	DDX_Control(pDX, RE1, m_ebRealis1);
	DDX_Control(pDX, IMAG1, m_ebImag1);
	DDX_Control(pDX, RE2, m_ebRealis2);
	DDX_Control(pDX, IMAG2, m_ebImag2);
	DDX_Control(pDX, RE3, m_ebRealis3);
	DDX_Control(pDX, IMAG3, m_ebImag3);
	DDX_Control(pDX, RESULT1, m_ebResult1);
	DDX_Control(pDX, RESULT2, m_ebResult2);
}

BEGIN_MESSAGE_MAP(CComplexCalcDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(THEBUTTON, &CComplexCalcDlg::OnBnClickedThebutton)
	ON_BN_CLICKED(IDC_CLEARBUTTON, &CComplexCalcDlg::OnBnClickedClearbutton)
	ON_BN_CLICKED(IDC_RADIO1, &CComplexCalcDlg::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &CComplexCalcDlg::OnBnClickedRadio2)
	ON_BN_CLICKED(IDC_RADIO3, &CComplexCalcDlg::OnBnClickedRadio3)
	ON_BN_CLICKED(IDC_RADIO4, &CComplexCalcDlg::OnBnClickedRadio4)
END_MESSAGE_MAP()


// CComplexCalcDlg message handlers

BOOL CComplexCalcDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	m_bRadio1.SetCheck(1);			//domy�lnie zaznaczone dodawanie
	m_bComboBox.SetCurSel(0);		//domy�lnie jest modu�
	m_nButton = 1;					//domy�lnie dodawanie

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CComplexCalcDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CComplexCalcDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CComplexCalcDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CComplexCalcDlg::OnBnClickedThebutton()
{
	//wczytanie wszystkich liczb
	CString s = (L"");
	m_ebRealis1.GetWindowTextW(s);
	double re1 = _wtof(s);
	m_ebRealis2.GetWindowTextW(s);
	double re2 = _wtof(s);
	m_ebRealis3.GetWindowTextW(s);
	double re3 = _wtof(s);
	m_ebImag1.GetWindowTextW(s);
	double im1 = _wtof(s);
	m_ebImag2.GetWindowTextW(s);
	double im2 = _wtof(s);
	m_ebImag3.GetWindowTextW(s);
	double im3 = _wtof(s);

	//obliczenie
	ComplexNum cmplx1 = ComplexNum(re1, im1);
	ComplexNum cmplx2 = ComplexNum(re2, im2);
	ComplexNum cmplx3 = ComplexNum(re3, im3);

	ComplexNum result1;
	switch (m_nButton)
	{
	case 1:
		result1 = cmplx1 + cmplx2;
		break;
	case 2:
		result1 = cmplx1 - cmplx2;
		break;
	case 3:
		result1 = cmplx1 * cmplx2;
		break;
	case 4:
		result1 = cmplx1 / cmplx2;
		break;
	}
	m_ebResult1.SetWindowTextW(result1.ToLCString());

	int sel = m_bComboBox.GetCurSel();

	CString str;

	switch(sel)
	{
	case 0:
		double result2;
		result2 = cmplx3.Modulus();
		str.Format(L"%g", result2);
		m_ebResult2.SetWindowTextW(str);
		break;
	case 1:
		ComplexNum result22;
		result22 = cmplx3.Conjungate();
		m_ebResult2.SetWindowTextW(result22.ToLCString());
		break;
	}

}


void CComplexCalcDlg::OnBnClickedClearbutton()
{
	m_ebRealis1.SetWindowTextW(L"");
	m_ebRealis2.SetWindowTextW(L"");
	m_ebRealis3.SetWindowTextW(L"");

	m_ebImag1.SetWindowTextW(L"");
	m_ebImag2.SetWindowTextW(L"");
	m_ebImag3.SetWindowTextW(L"");

	m_ebResult1.SetWindowTextW(L"");
	m_ebResult2.SetWindowTextW(L"");
}


void CComplexCalcDlg::OnBnClickedRadio1()
{
	m_nButton = 1;
}


void CComplexCalcDlg::OnBnClickedRadio2()
{
	m_nButton = 2;
}


void CComplexCalcDlg::OnBnClickedRadio3()
{
	m_nButton = 3;
}


void CComplexCalcDlg::OnBnClickedRadio4()
{
	m_nButton = 4;
}
