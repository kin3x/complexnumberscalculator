#include "stdafx.h"
#include "ComplexNum.h"



ComplexNum::ComplexNum()
{
	m_nRealis = 0;
	m_nImaginalis = 0;
}

ComplexNum::ComplexNum(double realis, double imaginalis)
{
	m_nRealis = realis;
	m_nImaginalis = imaginalis;
}

ComplexNum::~ComplexNum()
{
}

void ComplexNum::SetRealis(double realis)
{
	m_nRealis = realis;
}

void ComplexNum::SetImaginalis(double imaginalis)
{
	m_nImaginalis = imaginalis;
}

double ComplexNum::GetRealis() const
{
	return m_nRealis;
}

double ComplexNum::GetImaginalis() const
{
	return m_nImaginalis;
}

double ComplexNum::Modulus() const
{
	return sqrt(m_nRealis*m_nRealis + m_nImaginalis*m_nImaginalis);
}

ComplexNum ComplexNum::Conjungate() const
{
	return ComplexNum(m_nRealis, m_nImaginalis * (-1));
}

CString ComplexNum::ToLCString() const
{
	CString str;

	if (m_nImaginalis > 0)
		str.Format(L"%g+%gi", m_nRealis, m_nImaginalis);
	else
		str.Format(L"%g-%gi", m_nRealis, abs(m_nImaginalis));

	return str;
}

ComplexNum operator + (const ComplexNum & left, const ComplexNum & right)
{
	return ComplexNum(left.m_nRealis + right.m_nRealis, left.m_nImaginalis + right.m_nImaginalis);
}

ComplexNum operator - (const ComplexNum & left, const ComplexNum & right)
{
	return ComplexNum(left.m_nRealis - right.m_nRealis, left.m_nImaginalis - right.m_nImaginalis);
}

ComplexNum operator * (const ComplexNum & left, const ComplexNum & right)
{
	double re = left.m_nRealis * right.m_nRealis - left.m_nImaginalis * right.m_nImaginalis;
	double im = left.m_nImaginalis * right.m_nRealis + left.m_nRealis * right.m_nImaginalis;

	return ComplexNum(re, im);
}

ComplexNum operator / (const ComplexNum & left, const ComplexNum & right)
{
	double re = (left.m_nRealis * right.m_nRealis + left.m_nImaginalis * right.m_nImaginalis) / (right.m_nRealis * right.m_nRealis + right.m_nImaginalis * right.m_nImaginalis);
	double im = (left.m_nImaginalis * right.m_nRealis - left.m_nRealis * right.m_nImaginalis) / (right.m_nRealis * right.m_nRealis + right.m_nImaginalis * right.m_nImaginalis);

	return ComplexNum(re,im);
}

ComplexNum & operator += (ComplexNum & left, const ComplexNum & right)
{
	left.m_nRealis += right.m_nRealis;
	left.m_nImaginalis += right.m_nImaginalis;

	return left;
}

ComplexNum & operator -= (ComplexNum & left, const ComplexNum & right)
{
	left.m_nRealis -= right.m_nRealis;
	left.m_nImaginalis -= right.m_nImaginalis;

	return left;
}

ComplexNum & operator *= (ComplexNum & left, const ComplexNum & right)
{
	double re = left.m_nRealis * right.m_nRealis - left.m_nImaginalis * right.m_nImaginalis;
	double im = left.m_nImaginalis * right.m_nRealis + left.m_nRealis * right.m_nImaginalis;

	left.m_nRealis = re;
	left.m_nImaginalis = im;

	return left;
}

ComplexNum & operator /= (ComplexNum & left, const ComplexNum & right)
{
	double re = (left.m_nRealis * right.m_nRealis + left.m_nImaginalis * right.m_nImaginalis) / (right.m_nRealis * right.m_nRealis + right.m_nImaginalis * right.m_nImaginalis);
	double im = (left.m_nImaginalis * right.m_nRealis - left.m_nRealis * right.m_nImaginalis) / (right.m_nRealis * right.m_nRealis + right.m_nImaginalis * right.m_nImaginalis);

	left.m_nRealis = re;
	left.m_nImaginalis = im;

	return left;
}

std::istream & operator >> (std::istream & s, ComplexNum & right)
{
	std::cout << "Type real part of complex number:\n";
	s >> right.m_nRealis;
	std::cout << "Type imaginary part of complex number:\n";
	s >> right.m_nImaginalis;
	
	return s;
}

std::ostream & operator<<(std::ostream & s, ComplexNum & right)
{
	if (right.m_nImaginalis > 0)
		return s << right.m_nRealis << "+" << right.m_nImaginalis << "i";
	else
		return s << right.m_nRealis << "-" << abs(right.m_nImaginalis) << "i";
}





