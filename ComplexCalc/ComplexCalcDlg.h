
// ComplexCalcDlg.h : header file
//

#pragma once
#include "afxwin.h"


// CComplexCalcDlg dialog
class CComplexCalcDlg : public CDialogEx
{
// Construction
public:
	CComplexCalcDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_COMPLEXCALC_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedThebutton();
	afx_msg void OnBnClickedClearbutton();
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
	afx_msg void OnBnClickedRadio3();
	afx_msg void OnBnClickedRadio4();
private:
	CButton m_bRadio1;
	CString m_sComboBoxValue;
	CComboBox m_bComboBox;
	CString m_sRealis1;
	CEdit m_ebRealis1;
	CEdit m_ebImag1;
	CEdit m_ebRealis2;
	CEdit m_ebImag2;
	CEdit m_ebRealis3;
	CEdit m_ebImag3;
	CEdit m_ebResult1;
	CEdit m_ebResult2;
	int m_nButton;
};
